#!/bin/bash

set -e

# dump databases
for DATABASE in `psql -At -U $POSTGRES_USER -c "select datname from pg_database where not datistemplate order by datname;" $POSTGRES_USER`
do
  echo "Plain backup of $DATABASE"
  pg_dump -U $POSTGRES_DB -Fc "$DATABASE" > /opt/backups/"$DATABASE".$(date -d "today" +"%Y-%m-%d-%H-%M").dump
done

# delete files older than 7 days
find /opt/backups -mtime +7 -type f -delete

