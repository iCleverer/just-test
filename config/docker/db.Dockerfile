FROM postgres:12

ENV TZ Europe/Moscow

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
  postgresql-server-dev-$PG_MAJOR \
  unzip \
  make \
  && apt-get purge -y --auto-remove postgresql-server-dev-$PG_MAJOR make unzip

# set time zone
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# add backup job
RUN mkdir -p /opt/backups
COPY ./config/bash/pgbackup.sh /opt/pgbackup.sh
RUN chmod +x /opt/pgbackup.sh

# add init script
RUN mkdir -p /docker-entrypoint-initdb.d
COPY ./config/bash/initdb.sh /docker-entrypoint-initdb.d/initdb.sh
EXPOSE 5432
# create volume for backups
VOLUME ["/opt/backups"]
