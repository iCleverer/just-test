## Just-Work.org. Test task

### Note

Admin credentials:

login - admin

password - admin
### How to use
`docker-compose up autotests` to run autotests

`docker-compose up runserver` to run the test task 
### API examples

End-point: `GET /api/v1/pages/`

Result: 
```json
{
    "count": 2,
    "total_pages": 1,
    "next": false,
    "previous": false,
    "results": [
        {
            "title": "How to use print",
            "detailed_url": "/api/v1/pages/2/"
        },
        {
            "title": "Python Blog Introduction",
            "detailed_url": "/api/v1/pages/1/"
        }
    ]
}
```
End-point: `GET /api/v1/pages/1/`

Result: 
```json
{
    "title": "Python Blog Introduction",
    "source_page": [
        {
            "title": "History of Lorem Ipsum and What It Means",
            "counter": 28,
            "order": 0,
            "source_url": "https://www.youtube.com/watch?v=CmzKQ3PSrow",
            "subtitles_url": "",
            "resourcetype": "Video"
        },
        {
            "title": "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..",
            "counter": 28,
            "order": 1,
            "source_text": "Phasellus cursus tellus velit, id fermentum lorem accumsan at. Integer fringilla dolor nec leo molestie, sit amet congue purus tincidunt. Pellentesque quis risus eu libero ullamcorper vulputate ac non urna. Suspendisse potenti. Aliquam in lacinia ex. Fusce consectetur sit amet magna ut aliquam. Fusce elementum nisi euismod arcu dignissim efficitur. Etiam viverra, nibh vitae pharetra facilisis, dolor nisi posuere ipsum, id vehicula purus turpis sit amet enim.\r\n\r\nNunc blandit eleifend risus sed gravida. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam eleifend scelerisque purus, sed fermentum sem vulputate rutrum. Morbi et libero ex. Etiam finibus sed lacus ut dictum. Morbi a pulvinar ante. Nulla tincidunt, elit nec facilisis feugiat, urna arcu accumsan tortor, in tempus augue mi ut arcu.",
            "resourcetype": "Text"
        }
    ]
}
```