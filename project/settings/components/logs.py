from pathlib import Path

from settings.components import BASE_DIR

LOGS_ROOT = BASE_DIR.joinpath("logs")
Path(LOGS_ROOT).mkdir(parents=True, exist_ok=True)

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "require_debug_false": {"()": "django.utils.log.RequireDebugFalse"},
        "require_debug_true": {"()": "django.utils.log.RequireDebugTrue"},
    },
    "root": {"level": "DEBUG", "handlers": ["console"]},
    "formatters": {
        "simple": {
            "format": "[%(asctime)s] %(levelname)s: %(message)s",
            "datefmt": "%d/%m/%Y %H:%M:%S",
        },
        "verbose": {
            "format": "%(levelname)s %(asctime)s %(pathname)s %(funcName)s %(lineno)d %(process)d %(thread)d %(message)s",
            "datefmt": "%d/%m/%Y %H:%M:%S",
        },
        "custom": {
            "format": "%(levelname)s %(asctime)s %(pathname)s:%(lineno)d %(message)s",
            "datefmt": "%d/%m/%Y %H:%M:%S",
        },
    },
    "handlers": {
        "null": {"level": "DEBUG", "class": "logging.NullHandler"},
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
        "mail_admins": {
            "level": "ERROR",
            "class": "django.utils.log.AdminEmailHandler",
            "include_html": True,
            "filters": ["require_debug_false"],
        },
        "main_file": {
            "level": "INFO",
            "class": "logging.handlers.RotatingFileHandler",
            "formatter": "simple",
            "filename": LOGS_ROOT.joinpath("main.log"),
        },
    },
    "loggers": {
        "django.db.backends": {
            "handlers": ["null"],
            "level": "DEBUG",
            "propagate": False,
        },
        "django.template": {
            "handlers": ["console"],
            "level": "INFO",
            "propagate": False,
        },
        "django.security.DisallowedHost": {"handlers": ["console"], "propagate": False},
        "main": {"handlers": ["main_file"], "level": "INFO", "propagate": False},
    },
}
