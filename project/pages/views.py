from rest_framework import mixins
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from pages.models import Page
from pages.serializers import PageSerializer, PageDetailSerializer
from pages.tasks import increase_counter


class PagesViewSet(
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    GenericViewSet,
):
    queryset = Page.objects.all()
    serializer_class = PageSerializer

    def get_serializer_class(self):
        if self.action == "list":
            return PageSerializer
        return PageDetailSerializer

    def retrieve(self, request, *args, **kwargs):
        increase_counter.delay(kwargs.get('pk'))
        return super(PagesViewSet, self).retrieve(request, *args, **kwargs)
