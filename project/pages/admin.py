from django.contrib import admin
from django.contrib.admin import ModelAdmin
from polymorphic.admin import (
    PolymorphicChildModelAdmin,
    PolymorphicParentModelAdmin,
    StackedPolymorphicInline,
    PolymorphicInlineSupportMixin,
)

from pages.models import Page, BaseContent, Video, Audio, Text


class BaseContentInline(StackedPolymorphicInline):
    class VideoInline(StackedPolymorphicInline.Child):
        model = Video
        readonly_fields = ("counter",)

    class AudioInline(StackedPolymorphicInline.Child):
        model = Audio
        readonly_fields = ("counter",)

    class TextInline(StackedPolymorphicInline.Child):
        model = Text
        readonly_fields = ("counter",)

    model = BaseContent
    child_inlines = (
        VideoInline,
        AudioInline,
        TextInline,
    )
    ordering = ("order",)


@admin.register(Page)
class PageAdmin(PolymorphicInlineSupportMixin, ModelAdmin):
    inlines = (BaseContentInline,)
    # Должен поддерживаться поиск по заголовку (title) страниц и контента (по частичному совпадению от начала)
    # Если правильно понял текст выше, что только у контента title нужно искать от начала source_page__title__startswith
    search_fields = ("title", "source_page__title__startswith")


class BaseContentChildAdmin(PolymorphicChildModelAdmin):
    base_model = BaseContent
    readonly_fields = ("counter", "source_page")


@admin.register(Video)
class VideoAdmin(BaseContentChildAdmin):
    base_model = Video


@admin.register(Audio)
class AudioAdmin(BaseContentChildAdmin):
    base_model = Audio


@admin.register(Text)
class TextAdmin(BaseContentChildAdmin):
    base_model = Text


@admin.register(BaseContent)
class BaseContentParentAdmin(PolymorphicParentModelAdmin):
    base_model = BaseContent
    child_models = (
        Video,
        Audio,
        Text,
    )
