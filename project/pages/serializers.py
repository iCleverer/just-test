from rest_framework import serializers
from rest_framework.reverse import reverse
from rest_polymorphic.serializers import PolymorphicSerializer

from pages.models import Page, Video, Audio, Text, BaseContent


class BaseContentSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ("title", "counter", "order")


class VideoSerializer(BaseContentSerializer):
    class Meta:
        model = Video
        fields = BaseContentSerializer.Meta.fields + (
            "source_url",
            "subtitles_url",
        )


class AudioSerializer(BaseContentSerializer):
    class Meta:
        model = Audio
        fields = BaseContentSerializer.Meta.fields + ("bitrate",)


class TextSerializer(BaseContentSerializer):
    class Meta:
        model = Text
        fields = BaseContentSerializer.Meta.fields + ("source_text",)


class ContentSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        Video: VideoSerializer,
        Audio: AudioSerializer,
        Text: TextSerializer,
    }

    class Meta:
        model = BaseContent


class PageSerializer(serializers.ModelSerializer):
    detailed_url = serializers.SerializerMethodField()

    class Meta:
        model = Page
        fields = ("title", "detailed_url")

    @staticmethod
    def get_detailed_url(obj):
        return reverse("pages:base:page-detail", args=[obj.id])


class PageDetailSerializer(serializers.ModelSerializer):
    source_page = ContentSerializer(many=True)

    class Meta:
        model = Page
        fields = ("title", "source_page")
