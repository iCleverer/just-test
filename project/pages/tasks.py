from core.celery import app


@app.task
def increase_counter(obj_id):
    from pages.models import BaseContent
    from django.db.models import F

    if obj_id:
        BaseContent.objects.filter(source_page__pk=obj_id).update(counter=F('counter') + 1)
