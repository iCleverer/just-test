from django.urls import include, path
from rest_framework import routers

from pages.views import PagesViewSet

router = routers.DefaultRouter()
router.register("", PagesViewSet, basename="page")


urlpatterns = [path(r"", include((router.urls, "base")))]
