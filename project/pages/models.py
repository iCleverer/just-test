from django.db import models
from polymorphic.models import PolymorphicModel

from settings.components.constants import MAX_TITLE_LENGTH


class Page(models.Model):
    title = models.CharField(max_length=MAX_TITLE_LENGTH)

    class Meta:
        verbose_name = "Page"
        ordering = ("-id",)

    def __str__(self):
        return self.title


class BaseContent(PolymorphicModel):
    counter = models.PositiveBigIntegerField(default=0)
    title = models.CharField(max_length=MAX_TITLE_LENGTH)
    order = models.PositiveBigIntegerField(default=0)
    source_page = models.ForeignKey(
        Page, related_name="source_page", on_delete=models.DO_NOTHING, null=True
    )

    class Meta:
        verbose_name = "Content"
        ordering = ("order",)

    def __str__(self):
        return self.title


class Video(BaseContent):
    source_url = models.URLField()
    subtitles_url = models.URLField(blank=True)


class Audio(BaseContent):
    bitrate = models.DecimalField(max_digits=15, decimal_places=0)


class Text(BaseContent):
    source_text = models.TextField()
