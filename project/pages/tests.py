from collections import OrderedDict
from random import randint

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from .models import Page, Audio, Text, Video


class ListPagesTest(APITestCase):
    def setUp(self):
        page = Page.objects.create(title=f"Test pages #0")
        page = Page.objects.create(title=f"Test pages #1")
        Audio.objects.create(
            title="Audio test page #00",
            source_page=page,
            order=0,
            bitrate=100,
        )
        Text.objects.create(
            title="Text test page #01",
            source_page=page,
            order=0,
            source_text="This is a text for text test page #01",
        )
        Video.objects.create(
            title="Video test page #01",
            source_page=page,
            order=0,
            source_url="https://youtube.com/",
        )

    def test_pages_view_list(self):
        url = reverse("pages:base:page-list")
        response = self.client.get(url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data.get("results")), 2)

    def test_detail_page_view(self):
        url = reverse("pages:base:page-detail", args=[2])
        response = self.client.get(url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data.get("source_page")), 3)
