from rest_framework import pagination
from rest_framework.response import Response


class BaseCustomPagination(pagination.PageNumberPagination):
    page_size_query_param = 'page_size'
    max_page_size = 50

    def get_paginated_response(self, data):
        return Response(
            {
                "count": self.page.paginator.count,
                "total_pages": self.page.paginator.num_pages,
                "next": self.page.has_next(),
                "previous": self.page.has_previous(),
                "results": data,
            }
        )
