from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/v1/pages/", include(("pages.urls", "pages"), namespace="pages")),
]
